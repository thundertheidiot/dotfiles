;; -*- byte-compile-warnings: (not free-vars callargs unresolved) -*-

;; init.el
;; This file bootstraps loading of the org config
;; Config.org is first exported as an org file, which causes the resulting org file to contain all of the #+INCLUDE blocks.
;; After this step the temporary org export is tangled to elisp with org-babel-tangle-file, and then byte-compiled
;; This only happens if config.el does not exist, or config.org is newer than config.el


(setq config-org (concat user-emacs-directory "config.org"))
(setq config-el (concat user-emacs-directory "config.el"))

(require 'ob-tangle)

(defun compile-org-to-elisp (input-file output)
  "Tangle org file (including imports) and byte-compile it"
  (interactive "Input file: " "Output file: ")
  (let ((temp-export (concat input-file ".org")))
	(call-process "emacs" nil nil nil "-Q" "--batch" input-file "--eval" "(org-org-export-to-org)")
	(org-babel-tangle-file temp-export output)
	(delete-file temp-export)
	(byte-compile-file output)
  )
)

(if (file-exists-p config-el)
  (if (file-newer-than-file-p config-org config-el)
	(compile-org-to-elisp config-org config-el))
  (compile-org-to-elisp config-org config-el))

(load config-el)
