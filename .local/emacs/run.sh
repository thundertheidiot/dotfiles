#!/bin/sh

if [ "$1" = "exwm" ]; then
    shift
    Xephyr -screen 1280x800 -resizeable :420 &
    export DISPLAY=:420
    emacs -q --load ~/.local/emacs/init.el --init-directory ~/.local/emacs/ $*
else
    emacs -q --load ~/.local/emacs/init.el --init-directory ~/.local/emacs/ $*;
fi
