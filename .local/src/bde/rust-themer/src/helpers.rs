use std::fs;
use std::env;

pub fn substitute_string_in_file(filename: &str, from: &str, to: &str) {
    let string = fs::read_to_string(filename).expect("Unable to read file");

    let s2 = string.replace(from, to);

    fs::write(filename, s2).expect("Unable to write");
}

pub fn color_substitution_on_file(filename: &str, xresources: &Vec<String>) {
    for i in 0..15 {
        let from = format!("{}{}", "#color_", i.to_string());
        let to = xresources[i].as_str();
        substitute_string_in_file(filename, from.as_str(), to);
    }

    substitute_string_in_file(filename, "#color_bg", xresources[16].as_str());
    substitute_string_in_file(filename, "#color_fg", xresources[17].as_str());
    substitute_string_in_file(filename, "#color_main", xresources[18].as_str());
}

pub fn get_themer_directory() -> String {
    let c: String = env::var("XDG_CONFIG_HOME").unwrap_or("".to_string());
    let mut path: String;

    if c != "" {
       path = c; 
    } else {
        let h: String = env::var("HOME").unwrap();
        path = h + "/.config/";
    }

    path += "/themer/";

    return path;
}

pub fn construct_filename(filename: &str) -> String {
    let h = env::var("HOME").unwrap();
    let c = env::var("XDG_CONFIG_HOME").unwrap_or(format!("{}{}", h, "/.config/"));
    let t = get_themer_directory();

    let mut s: String;
    s = filename.replace("_HOME_", h.as_str());
    s = s.replace("_CONFIG_", c.as_str());
    s = s.replace("_THEMER_", t.as_str());

    return s;

}
