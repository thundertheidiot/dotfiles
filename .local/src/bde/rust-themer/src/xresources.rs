use x11::xlib;
use x11::xlib::{XOpenDisplay,XrmGetStringDatabase,XResourceManagerString,XrmValue,XrmGetResource};
use std::ffi::CString;
use std::ffi::CStr;
use std::ptr;
use std::slice;
use std::vec::Vec;
use std::env;
use std::fs;
use std::process::Command;
#[ path = "./config.rs" ] mod config;
#[ path = "./helpers.rs" ] mod helpers;

unsafe fn get_key_value(xrdb: xlib::XrmDatabase, key: CString) -> String {
    let mut value: XrmValue = std::mem::zeroed();

    XrmGetResource(xrdb, key.as_ptr(), ptr::null_mut(), &mut ptr::null_mut(), &mut value);

    // let v: XrmValue = value;

    let slice = slice::from_raw_parts(value.addr as *const u8, value.size as usize);
    let cstr = CStr::from_bytes_with_nul_unchecked(slice);
    let string = cstr.to_string_lossy().into_owned();

    string.replace("#", "")

    // return string_from_xrm_value(&value);

}

pub fn get_xresources() -> Vec<String> {

    let mut xresources: Vec<String> = Vec::new();

    unsafe {
        let display = XOpenDisplay(ptr::null());
        if display.is_null() {
            return xresources;
        }

        let xrdb = XrmGetStringDatabase(XResourceManagerString(display));

        if xrdb.is_null() {
            return xresources;
        }

        // Rust weird???
        for i in 0..16 {
            let k = CString::new(format!("color{}", i)).unwrap();

            xresources.push(get_key_value(xrdb, k));
        }

        let names = ["background", "foreground", "maincolor"];
        for name in names {
            let k = CString::new(name).unwrap();
            xresources.push(get_key_value(xrdb, k));
        }

        return xresources;

        
    }

}

fn get_xresources_file_path() -> String {
    let h = env::var("HOME").unwrap(); 
    return env::var("XRESOURCES").unwrap_or(h + "/.Xresources");
}

pub fn change_xresources_theme(theme: &crate::config::Theme) {
    let xf = get_xresources_file_path(); 

    let content = fs::read_to_string(&xf).expect("Unable to read xresources file");

    let bottom = content.splitn(2, "!! THEMER END !!").nth(1).unwrap_or("");

    let mut new_content = format!("#define BACKGROUND {}\n", theme.background);
    new_content += format!("#define FOREGROUND {}\n", theme.foreground).as_str();

    for i in 0..16 {
       new_content += format!("#define COLOR{} {}\n", i, theme.colors[i]).as_str(); 
    }

    new_content += format!("#define MAINCOLOR {}\n", theme.maincolor).as_str();
    new_content += "!! THEMER END !!\n";

    new_content += bottom;

    fs::write(&xf, new_content).expect("Unable to write xresources file");

}

pub fn load_xresources(config: &crate::config::ThemerConfig) {
    let xf: String;

    let p = helpers::construct_filename(&config.pywal_xresources_file.as_str());

    if config.pywal_enabled && !p.is_empty() {
        xf = p;
    } else {
        let theme: &crate::config::Theme = crate::config::pick_theme(&config.themes);
        change_xresources_theme(theme);
        xf = get_xresources_file_path();
    }

    let args = ["-load", xf.as_str()];

    let status = Command::new("xrdb")
        .args(args)
        .output()
        // .status()
        .expect("xrdb could not be executed");

    // println!("{:?}", status.stdout);

    // if !status.success() {
    //     println!("Loading xresources failed");
    // }

}
