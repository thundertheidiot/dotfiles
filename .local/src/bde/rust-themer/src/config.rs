use std::fs;
use std::io;
use toml::Value;
#[ path = "./helpers.rs" ] mod helpers;

pub struct Template {
    pub filename: String,
    pub content_file: String,
}

pub struct Theme {
    pub name: String,
    pub background: String,
    pub foreground: String,
    pub maincolor: String,
    // pub color_0: String,
    // pub color_1: String,
    // pub color_2: String,
    // pub color_3: String,
    // pub color_4: String,
    // pub color_5: String,
    // pub color_6: String,
    // pub color_7: String,
    // pub color_8: String,
    // pub color_9: String,
    // pub color_10: String,
    // pub color_11: String,
    // pub color_12: String,
    // pub color_13: String,
    // pub color_14: String,
    // pub color_15: String,
    pub colors: Vec<String>,
}

pub struct ThemerConfig {
    pub pywal_enabled: bool,
    pub pywal_xresources_file: String,
    pub files_installed: bool,
    pub templates: Vec<Template>,
    pub themes: Vec<Theme>,
    pub gtk_directory: String,
    pub gtk_destination: String,
}

impl ThemerConfig {
    fn new() -> ThemerConfig {
        ThemerConfig {
            pywal_enabled : false,
            pywal_xresources_file : "".to_string(),
            files_installed : false,
            templates : Vec::new(),
            themes : Vec::new(),
            gtk_directory: String::new(),
            gtk_destination: String::new(),
        }
    }
}

pub fn get_themer_config_file() -> String {
    let dir: String = helpers::get_themer_directory();
    let file: String = format!("{}{}", dir, "/config.toml");

    return file;
}

pub fn get_themer_config() -> ThemerConfig {
    let content = fs::read_to_string(get_themer_config_file()).expect("Failed to read config");

    let toml = content.parse::<Value>().unwrap();

    let mut c = ThemerConfig::new();
    c.pywal_enabled = toml["themer"]["pywal_enabled"].as_bool().unwrap_or(false);
    c.pywal_xresources_file = toml["themer"]["pywal_xresources_file"].as_str().unwrap_or("").to_owned();
    c.files_installed = toml["themer"]["files_installed"].as_bool().unwrap_or(false);
    c.gtk_directory = toml["gtk"]["directory"].as_str().unwrap_or("").to_owned();
    c.gtk_destination = toml["gtk"]["destination"].as_str().unwrap_or("").to_owned();
    c.templates = get_templates();
    c.themes = get_themes();

    return c;
}

pub fn get_templates() -> Vec<Template> {
    let content = fs::read_to_string(get_themer_config_file()).expect("Failed to read config");
    let value: toml::Value = toml::from_str(content.as_str()).unwrap();

    let templates = match value.get("template") {
        Some(Value::Array(arr)) => {
            arr.iter()
                .map(|v| Template {
                    filename: helpers::construct_filename(v.get("filename").unwrap().as_str().unwrap()),
                    content_file: helpers::construct_filename(v.get("content_file").unwrap().as_str().unwrap()),
                })
                .collect::<Vec<_>>()
        }
        _ => panic!("Invalid TOML format"),
    };

    return templates;

}

pub fn get_themes() -> Vec<Theme> {
    let content = fs::read_to_string(get_themer_config_file()).expect("Failed to read config");
    let value: toml::Value = toml::from_str(content.as_str()).unwrap();

    let themes = match value.get("theme") {
        Some(Value::Array(arr)) => {
            arr.iter()
                .map(|v| Theme {
                    name: v.get("name").unwrap().as_str().unwrap().to_owned(),
                    foreground: v.get("foreground").unwrap().as_str().unwrap().to_owned(),
                    background: v.get("background").unwrap().as_str().unwrap().to_owned(),
                    maincolor: v.get("maincolor").unwrap().as_str().unwrap().to_owned(),
                    // color_0: v.get("color_0").unwrap().as_str().unwrap().to_owned(),
                    // color_1: v.get("color_1").unwrap().as_str().unwrap().to_owned(),
                    // color_2: v.get("color_2").unwrap().as_str().unwrap().to_owned(),
                    // color_3: v.get("color_3").unwrap().as_str().unwrap().to_owned(),
                    // color_4: v.get("color_4").unwrap().as_str().unwrap().to_owned(),
                    // color_5: v.get("color_5").unwrap().as_str().unwrap().to_owned(),
                    // color_6: v.get("color_6").unwrap().as_str().unwrap().to_owned(),
                    // color_7: v.get("color_7").unwrap().as_str().unwrap().to_owned(),
                    // color_8: v.get("color_8").unwrap().as_str().unwrap().to_owned(),
                    // color_9: v.get("color_9").unwrap().as_str().unwrap().to_owned(),
                    // color_10: v.get("color_10").unwrap().as_str().unwrap().to_owned(),
                    // color_11: v.get("color_11").unwrap().as_str().unwrap().to_owned(),
                    // color_12: v.get("color_12").unwrap().as_str().unwrap().to_owned(),
                    // color_13: v.get("color_13").unwrap().as_str().unwrap().to_owned(),
                    // color_14: v.get("color_14").unwrap().as_str().unwrap().to_owned(),
                    // color_15: v.get("color_15").unwrap().as_str().unwrap().to_owned(),
                    colors: Vec::from([v.get("color_0").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_1").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_2").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_3").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_4").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_5").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_6").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_7").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_8").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_9").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_10").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_11").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_12").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_13").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_14").unwrap().as_str().unwrap().to_owned(),
                             v.get("color_15").unwrap().as_str().unwrap().to_owned()])
                })
                .collect::<Vec<_>>()
        }
        _ => panic!("Invalid TOML format"),
    };

    return themes;

}

pub fn pick_theme(themes: &Vec<Theme>) -> &Theme {
    for i in 0..themes.len() {
        println!("{}. {}", i, &themes[i].name);
    }
    println!("Pick theme: ");


    let mut input = String::new();
    
    io::stdin().read_line(&mut input)
        .expect("Failed to read input");

    let num = input.trim().parse::<usize>()
        .expect("Invalid input. Please enter a number.");

    return &themes[num];

}
