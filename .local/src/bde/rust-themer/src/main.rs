mod xresources;
mod helpers;
mod config;
mod template;
mod gtk_theme;

use std::process::Command;
use std::env;

fn refresh_settings() {
   Command::new("xfconf-query")
       .arg("-c")
       .arg("xsettings")
       .arg("-p")
       .arg("/Net/ThemeName")
       .arg("-s")
       .arg("Adwaita")
       .output().expect("Failed to set gtk theme");
   
   Command::new("xfconf-query")
       .arg("-c")
       .arg("xsettings")
       .arg("-p")
       .arg("/Net/ThemeName")
       .arg("-s")
       .arg("custom")
       .output().expect("Failed to set gtk theme");

   Command::new("awesome-client")
       .arg("'awesome.restart()'")
       .output().expect("Failed to reload awesomewm");

   Command::new("killall")
       .arg("-USR1")
       .arg("st")
       .output().expect("Failed to reload st");
}

fn main() {

    let mut get_pywal_status: bool = false;
    let args: Vec<String> = env::args().collect();

    for s in &args {
        if s == "--get-pywal-status" {
            get_pywal_status = true;
        }
    }

    let config: config::ThemerConfig = config::get_themer_config();

    if get_pywal_status {
        println!("{}", &config.pywal_enabled);
        return;
    }

    xresources::load_xresources(&config);

    let xres: Vec<String> = xresources::get_xresources();

    template::make_templates(&config, &xres);
    
    gtk_theme::make(&config, &xres);

    refresh_settings();

}
