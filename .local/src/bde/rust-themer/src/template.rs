#[ path = "./config.rs" ] mod config;
#[ path = "./helpers.rs" ] mod helpers;
// use config::Template;
// use config::ThemerConfig;
use std::fs;

pub fn make_templates(config: &crate::config::ThemerConfig, xresources: &Vec<String>) {
    for t in &config.templates {
        let content = fs::read_to_string(&t.content_file).expect("Failed to read content file"); 
        let filename = &t.filename;

        fs::write(filename, content).expect("Unable to write");

        helpers::color_substitution_on_file(filename.as_str(), xresources);
        
    }
}
