#include "setup_gtk_theme.h"
#include <string>
#include <filesystem>
#include <iostream>
#include "themerconfig.h"
#include "helpers/path.h"
#include "helpers/file.h"

bool setup_gtk_theme() {
	if (config_file_path.empty()) { return false; }

	if (!config) { config = cpptoml::parse_file(config_file_path); }

	auto gtk_table = config->get_table("gtk");
	if (!gtk_table) { return false; };

	std::string gtk_template_directory = construct_path(gtk_table->get_as<std::string>("directory").value_or(""));
	if (gtk_template_directory.empty()) { return false; }
	
	std::string gtk_destination_directory = construct_path(gtk_table->get_as<std::string>("destination").value_or(""));
	if (gtk_destination_directory.empty()) { return false; }

	std::filesystem::remove_all(gtk_destination_directory);
	std::filesystem::copy(gtk_template_directory, gtk_destination_directory, std::filesystem::copy_options::recursive);

	for (const auto& p: std::filesystem::recursive_directory_iterator(gtk_destination_directory)) {
		if (!std::filesystem::is_directory(p)) {
			color_substitution_on_file(p.path());
		}
	}

	std::filesystem::current_path(gtk_destination_directory + "/gtk-2.0/");
	std::system("python3 ./render-assets.py");
	
	std::filesystem::current_path(gtk_destination_directory + "/gtk-3.0/");
	std::system("python3 ./render-assets.py");

	return true;
}
