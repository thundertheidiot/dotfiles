#include "theme.h"
#include "themerconfig.h"
#include "cpptoml.h"

std::vector<Theme*> themes;

Theme* get_theme_by_name(std::string name, std::vector<Theme*> themes) {
	for (Theme* t : themes) {
		if (t->name == name) {
			return t;
		}
	}

	return NULL;
}

bool update_themes() {
	if (config_file_path.empty()) { return false; }

	if (!config) { config = cpptoml::parse_file(config_file_path); }

	auto themes_t = config->get_table_array("theme");

	themes = {};

	for (const auto &table : *themes_t) {
		Theme* t = new Theme;
		std::string name = table->get_as<std::string>("name").value_or("");
		if (name.empty()) { continue; }
		std::string background = table->get_as<std::string>("background").value_or("");
		if (background.empty()) { continue; }
		std::string foreground = table->get_as<std::string>("foreground").value_or("");
		if (foreground.empty()) { continue; }
		std::string maincolor = table->get_as<std::string>("maincolor").value_or("");
		if (maincolor.empty()) { continue; }

		std::vector<std::string> colors;
		for (int i = 0; i < 16; ++i) {
			std::string c = table->get_as<std::string>("color_" + std::to_string(i)).value_or("");
			if (c.empty()) { continue; }
			colors.push_back(c);
		}

		t->name = name;
		t->background = background;
		t->foreground = foreground;
		t->maincolor = maincolor;
		t->colors = colors;

		themes.push_back(t);
	}

	return true;


}
