#ifndef THEME_H
#define THEME_H
#include <vector>
#include <string>

class Theme {
	public:
		std::string name;
		std::vector<std::string> colors;
		std::string background;
		std::string foreground;
		std::string maincolor;
};

extern std::vector<Theme*> themes;

Theme* get_theme_by_name(std::string name, std::vector<Theme*> themes);

bool update_themes();

#endif
