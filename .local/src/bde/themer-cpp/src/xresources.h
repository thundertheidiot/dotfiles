#ifndef XRESOURCES_H
#define XRESOURCES_H

#include <string>
#include <vector>
#include "theme.h"

extern std::string xresources_file;
extern std::vector<std::string> xresources;

bool update_xresources();
bool load_xresources();
bool change_xresources_theme(Theme* theme);

#endif
