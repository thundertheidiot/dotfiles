#include "template.h"
#include "themerconfig.h"
#include "xresources.h"
#include "helpers/file.h"
#include "helpers/path.h"
#include "helpers/stringhelper.h"
#include <fstream>
#include <sstream>
#include <iostream>

std::vector<Template*> templates;

bool update_templates() {
	if (config_file_path.empty()) { return false; }

	if (!config) { config = cpptoml::parse_file(config_file_path); }

	auto templates_t = config->get_table_array("template");

	templates = {};

	for (const auto &table : *templates_t) {
		Template* t = new Template;

		std::string filename = table->get_as<std::string>("filename").value_or("");
		if (filename.empty()) { continue; }
		std::string content_file = table->get_as<std::string>("content_file").value_or("");
		if (content_file.empty()) { continue; }

		t->filename = construct_path(filename);
		t->content_file = construct_path(content_file);

		templates.push_back(t);
	}

	return true;
}

std::ifstream in;
std::stringstream buf;
std::ofstream out;

void subinfile(std::string filename, std::string from, std::string to) {
	std::ifstream input(filename);
	std::stringstream b; b << input.rdbuf();
	std::string content = b.str();

	content = substitute_string(content, from, to);
	
	input.close();
	std::ofstream output(filename);
	output << content << '\n';
	// std::cout << content << '\n';
	output.close();
}

void colsubinfile(std::string filename) {
	// if (xresources.size() != 19) { return false; }
	substitute_string_in_file(filename, "color_bg",	xresources[16]);
	substitute_string_in_file(filename, "color_fg",	xresources[17]);
	substitute_string_in_file(filename, "color_main",	xresources[18]);

	for (int i = 0; i < 16; ++i) {
		substitute_string_in_file(filename, "color_" + std::to_string(i), xresources[i]);
	}
}

bool make_templates() {
	if (xresources.size() == 0) { return false; }

	for (Template* t : templates) {
		// std::cout << t->content_file << "\n";
		// std::cout << t->filename << "\n";
		std::fstream in(t->content_file);
		std::stringstream buf;
		std::string content;
		buf << in.rdbuf();
		// if (!(buf << in.rdbuf())) { return false; }

		content = buf.str();

		std::ofstream out(t->filename);
		out << content;

		in.close();
		out.close();
		// if (!(out << content)) { return false; }

		color_substitution_on_file(t->filename);

	}

	return true;
}


