#ifndef THEMERCONFIG_H
#define THEMERCONFIG_H

#include <vector>
#include <string>
#include "cpptoml.h"


class ThemerConfig {
	public:
		bool pywal_enabled;
		std::string pywal_xresources_file;
		std::string gtk_template_directory;
		std::string gtk_destination_directory;
};

extern std::string config_file_path;
extern ThemerConfig* themer_config;
extern std::shared_ptr<cpptoml::table> config;

bool update_themer_config();

#endif
