#include "xresources.h"
#include "theme.h"
#include "helpers/stringhelper.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

std::string xresources_file;
std::vector<std::string> xresources;

extern "C" {
	#include <X11/Xlib.h>
	#include <X11/Xresource.h>
}

bool update_xresources() {
	Display* display = XOpenDisplay(NULL);
	XrmDatabase xrdb;
	XrmValue value;
	char* type;
	
	std::vector<std::string> return_value;

	if (display != NULL) {
		xrdb = XrmGetStringDatabase(XResourceManagerString(display));

		if (xrdb != NULL) {
		
			for (int i = 0; i < 16; ++i) {

				std::string k = "color" + std::to_string(i);
			
				XrmGetResource(xrdb, k.c_str(), NULL, &type, &value);
				return_value.push_back(substitute_string(value.addr, "#", ""));
				
			}
			
			XrmGetResource(xrdb, "background", NULL, &type, &value);
			return_value.push_back(substitute_string(value.addr, "#", ""));
			
			XrmGetResource(xrdb, "foreground", NULL, &type, &value);
			return_value.push_back(substitute_string(value.addr, "#", ""));
			
			XrmGetResource(xrdb, "maincolor", NULL, &type, &value);
			return_value.push_back(substitute_string(value.addr, "#", ""));
	
			xresources = return_value;
			return true;

		}
	

	}

	return false;

}

// TODO: bool
bool load_xresources() {
	if (xresources_file.empty()) { return false; }

	std::string xresources_command = "xrdb -load " + xresources_file;

	int c = std::system(xresources_command.c_str());

	if (c == 0) {
		return true;
	} else { return false; }
}

bool change_xresources_theme(Theme* theme) {
	if (xresources_file.empty()) { return false; }

	std::ifstream in(xresources_file);
	std::stringstream buf;
	std::string content;

	buf << in.rdbuf();
	// if (!(buf << in.rdbuf())) { return false; }

	content = buf.str();

	size_t pos = content.find("!! THEMER END !!");
	content.erase(0, pos);

	std::ofstream out(xresources_file);
	// if (!(out << "")) { return false; }

	out << "#define BACKGROUND " << theme->background << '\n';
	out << "#define FOREGROUND " << theme->foreground << '\n';

	for (int i = 0; i < 16; ++i) {
		out << "#define COLOR" << i << " " << theme->colors[i] << '\n';
	}
	
	out << "#define MAINCOLOR " << theme->maincolor << '\n';

	out << content << '\n';

	return true;
}
