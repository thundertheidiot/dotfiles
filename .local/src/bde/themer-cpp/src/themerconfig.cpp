#include "themerconfig.h"
#include "cpptoml.h"
#include <vector>
#include <string>
#include <iostream>
#include "helpers/path.h"

std::string config_file_path;
ThemerConfig* themer_config;
std::shared_ptr<cpptoml::table> config;

bool update_themer_config() {
	if (config_file_path.empty()) {
		return false;
	}

	if (!config) { config = cpptoml::parse_file(config_file_path); }
	auto themer_table = config->get_table("themer");

	ThemerConfig* c = new ThemerConfig;

	c->pywal_enabled = themer_table->get_as<bool>("pywal_enabled").value_or(false);
	c->pywal_xresources_file = construct_path(themer_table->get_as<std::string>("pywal_xresources_file").value_or(""));

	themer_config = c;
	return true;

}
