#ifndef PATH_H
#define PATH_H
#include <string>

std::string construct_path(std::string original);

#endif
