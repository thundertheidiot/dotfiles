#include "path.h"
#include <string>
#include "stringhelper.h"

std::string construct_path(std::string original) {
	std::string home = std::getenv("HOME");
	std::string config = std::getenv("XDG_CONFIG_HOME");

	if (config.empty()) { config = home + "/.config/"; }

	std::string themer = config + "/themer/";

	original = substitute_string(original, "_HOME_", home);
	original = substitute_string(original, "_CONFIG_", config);
	original = substitute_string(original, "_THEMER_", themer);

	return original;
}
