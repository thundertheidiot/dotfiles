#ifndef FILE_H
#define FILE_H
#include <string>

bool file_exists(std::string filename);

bool color_substitution_on_file(std::string filename);

#endif
