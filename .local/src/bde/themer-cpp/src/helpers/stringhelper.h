#ifndef STRINGHELPER_H
#define STRINGHELPER_H
#include <string>
#include <fstream>
#include <sstream>

std::string substitute_string(std::string string, std::string from, std::string to);
bool substitute_string_in_file(std::string filename, std::string from, std::string to);

#endif
