#include "stringhelper.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

std::string substitute_string(std::string string, std::string from, std::string to) {
	for (size_t pos = 0; (pos = string.find(from, pos)) != std::string::npos; pos += to.size()) {
		string.replace(pos, from.size(), to);
	}

	return string;
}

bool substitute_string_in_file(std::string filename, std::string from, std::string to) {
	std::ifstream input(filename);
	std::stringstream b; b << input.rdbuf();
	std::string content = b.str();

	content = substitute_string(content, from, to);
	
	input.close();
	std::ofstream output(filename);
	output << content << '\n';
	// std::cout << content << '\n';
	output.close();

	return true;
}


