#include "file.h"
#include "stringhelper.h"
#include "../xresources.h"
#include <string>
#include <fstream>
#include <iostream>

bool file_exists(std::string filename) {
	std::ifstream f(filename.c_str());
	return f.good();
}

bool color_substitution_on_file(std::string filename) {
	substitute_string_in_file(filename, "color_bg",	xresources[16]);
	substitute_string_in_file(filename, "color_fg",	xresources[17]);
	substitute_string_in_file(filename, "color_main",	xresources[18]);

	for (int i = 0; i < 16; ++i) {
		substitute_string_in_file(filename, "color_" + std::to_string(i), xresources[i]);
	}

	return true;
}

