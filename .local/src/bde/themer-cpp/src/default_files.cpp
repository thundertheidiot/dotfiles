#include "default_files.h"
#include <string>
#include <filesystem>
#include <iostream>
#include "themerconfig.h"
#include "helpers/path.h"
#include "helpers/stringhelper.h"

bool setup_default_files() {
	if (config_file_path.empty()) { return false; }
	if (!std::filesystem::is_regular_file(config_file_path)) { 
		std::ofstream o(config_file_path);
		o << "\n";
		o.close();
	}
	if (!config) { config = cpptoml::parse_file(config_file_path); }

	std::string directory;
	std::string destination;

	auto files = config->get_table("default_files");
	if (files) { 
		bool files_installed = files->get_as<bool>("files_installed").value_or(false);
		if (files_installed) { return true; }

		directory = construct_path(files->get_as<std::string>("directory").value_or(""));
		if (directory.empty()) { directory = construct_path("_THEMER_/defaults"); }

		destination = construct_path(files->get_as<std::string>("destination").value_or(""));
		if (destination.empty()) { destination = construct_path("_HOME_/"); }
	} else {
		directory = construct_path("_THEMER_/defaults");
		destination = construct_path("_HOME_/");
	}

	// TODO
	// use std::filesystem
	std::string command = "cp -r " + directory + "/. " + destination;
	std::system(command.c_str());

	substitute_string_in_file(config_file_path, "files_installed = false", "files_installed = true");

	// for (const auto& p : std::filesystem::recursive_directory_iterator(directory)) {
	// 	std::cout << substitute_string(p.path(), directory, "") << "\n";
	// 	// std::filesystem::copy(p.path(), destination, std::filesystem::copy_options::recursive | std::filesystem::copy_options::overwrite_existing);
	// }

	return true;
}
