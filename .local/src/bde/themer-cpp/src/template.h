#ifndef TEMPLATE_H
#define TEMPLATE_H
#include <string>
#include <vector>

class Template {
	public:
		std::string filename;
		std::string content_file;
};

extern std::vector<Template*> templates;

bool update_templates();

bool make_templates();

#endif
