#include <stdio.h>
#include <gtk/gtk.h>
#include "theme.h"
#include "themerconfig.h"
#include "xresources.h"
#include "template.h"
#include "helpers/path.h"
#include "ui.h"
#include "cpptoml.h"
#include <iostream>
#include <vector>
#include <string>
#include "helpers/file.h"
#include "helpers/stringhelper.h"
#include "setup_gtk_theme.h"
#include "default_files.h"

int main(int argc, char *argv[]) {

	bool run_defaults_check = false;
	bool get_pywal_status = false;

	for (int i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "--config") || !strcmp(argv[i], "-c")) {
			config_file_path = std::string(argv[++i]);
		}
		if (!strcmp(argv[i], "--defaults-check")) {
			run_defaults_check = true;
		}
		if (!strcmp(argv[i], "--get-pywal-status")) {
			get_pywal_status = true;
		}
	}

	if (config_file_path.empty()) {
		config_file_path = construct_path("_THEMER_/config.toml");
	}

	if (run_defaults_check) {
		setup_default_files();
		return 0;
	}
	
	update_themer_config();
	
	if (get_pywal_status) {
		if (themer_config->pywal_enabled) {
			std::cout << "true";
			return 0;
		} else {
			std::cout << "false";
			return 0;
		}
	}

	update_themes();

	if (themer_config->pywal_enabled && !themer_config->pywal_xresources_file.empty()) {
		xresources_file = themer_config->pywal_xresources_file;
	} else {
		std::string xresources_env = std::getenv("XRESOURCES");
		if (!xresources_env.empty()) {
			xresources_file = xresources_env;
		} else {
			xresources_env = construct_path("_HOME_/.Xresources");
		}

		Theme* chosen_theme = get_theme_by_name(pick_theme(), themes);
		change_xresources_theme(chosen_theme);
	}

	if (!file_exists(xresources_file)) {
		std::cout << "No valid xresources file found." << "\n";
		return 1;
	}

	load_xresources();
	update_xresources();
	update_templates();
	make_templates();
	setup_gtk_theme();

	//TODO make smarter
	// Custom commands from toml???

	std::system("xfconf-query -c xsettings -p /Net/ThemeName -s \"Adwaita\"");
	std::system("xfconf-query -c xsettings -p /Net/ThemeName -s \"custom\"");
	std::system("awesome-client 'awesome.restart()'");
	std::system("killall -USR1 st");

}
