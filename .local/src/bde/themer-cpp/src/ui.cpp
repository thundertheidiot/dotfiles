#include <gtk/gtk.h>
#include <vector>
#include <string>
#include <iostream>
#include "ui.h"
#include "theme.h"

std::string chosen_theme_name;
	
GtkWidget *win;

void clicked(GtkButton *btn, GtkWindow *win) {
	chosen_theme_name = gtk_button_get_label(btn);

	gtk_window_destroy(win);
}

static void app_activate (GApplication *app, gpointer *user_data) {

	win = gtk_application_window_new(GTK_APPLICATION(app));

	gtk_window_set_title(GTK_WINDOW(win), "Themer");
	gtk_window_set_default_size(GTK_WINDOW(win), 400, 300);
	gtk_window_present(GTK_WINDOW(win));

	auto box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_window_set_child(GTK_WINDOW(win), box);

	// auto group = gtk_toggle_button_new();

	for (Theme* t : themes) {
		auto btn = gtk_button_new_with_label(t->name.c_str());
		g_signal_connect(btn, "clicked", G_CALLBACK(clicked), win);

		gtk_box_append(GTK_BOX(box), btn);

		// auto btn = gtk_toggle_button_new_with_label(s.c_str());
		// gtk_toggle_button_set_group(GTK_TOGGLE_BUTTON(btn), GTK_TOGGLE_BUTTON(group));
		//
		// gtk_box_append(GTK_BOX(box), btn);
	}
}

std::string pick_theme() {
	GtkApplication *app;
	int stat;

	app = gtk_application_new("org.idiot.idiot", G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect(app, "activate", G_CALLBACK(app_activate), NULL);
	stat = g_application_run (G_APPLICATION (app), 0, NULL);
	g_object_unref (app);
	if ( stat == 0 ) {
		return chosen_theme_name;
	} else {
		return "";
	}
}
