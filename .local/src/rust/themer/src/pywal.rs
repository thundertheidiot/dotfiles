use which::which;
use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use crate::config::Theme;
use crate::config::Config;
use crate::finish;

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>> where P: AsRef<Path>, {
    let file = File::open(filename)?;
    return Ok(io::BufReader::new(file).lines());
}

pub fn main(config: &Config, maincolor: &usize) {
    which("wal").expect("Pywal not installed, unable to use pywal mode.");

    let cache_dir = env::var("XDG_CACHE_HOME")
        .unwrap_or(format!("{}{}", env::var("HOME").expect("Environment variable $HOME not set"), "/.cache/"));
    let colors_file = format!("{}{}", cache_dir, "/wal/colors");

    let mut colvec: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines(colors_file) {
        for line in lines {
            if let Ok(l) = line {
                colvec.push(l.to_string().replace("#", ""));
            }
        }
    }

    colvec.push(colvec[0].clone()); // Background
    colvec.push(colvec[7].clone()); // Foreground
    colvec.push(colvec[*maincolor].clone()); // Main 

    println!("{:?}", colvec);

    let theme: Theme = Theme {
        name : "Pywal".to_owned(),
        colors: colvec,
    };

    finish(config.clone(), theme); 

}
