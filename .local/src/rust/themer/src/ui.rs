use crate::config::Config;
use crate::config::Theme;
use crate::finish;
use gtk::Button;
use gtk::{prelude::*, Orientation};
use gtk::{glib, Application, ApplicationWindow};
use gtk::Box;

use mut_static::MutStatic;

lazy_static! {
    static ref CONFIG: MutStatic<Config> = MutStatic::new();
    static ref THEME: MutStatic<Theme> = MutStatic::new();
}

pub fn main(c: &Config) {
    let app = Application::builder().application_id("org.themer.Themer").build();

    CONFIG.set(c.clone()).unwrap();

    app.connect_activate(|app| build_ui(app));

    app.run();

    let theme: &Theme = &THEME.read().unwrap();

    if theme.colors.is_empty() {
        println!("No theme selected, exiting.");
        std::process::exit(1);
    }

    finish(CONFIG.read().unwrap().clone(), theme.clone());
}

fn build_ui(app: &Application) {

    let buttonbox = Box::new(Orientation::Vertical, 5);

    // let themes: Vec<Theme> = CONFIG.read().unwrap().themes.clone();
    // let themes: &Vec<Theme> = &CONFIG.read().unwrap().themes;

    for t in &CONFIG.read().unwrap().themes {
        let b = Button::builder().label(&t.name).build();
        b.connect_clicked(glib::clone!(@strong app => move |button| {
            let name: String = button.label().unwrap().to_string();

            for t in &CONFIG.read().unwrap().themes {
                if t.name == name {
                    app.quit();
                    THEME.set(t.clone()).unwrap();
                }
            }
            
        }));

        buttonbox.append(&b);
    }

    let window = ApplicationWindow::builder()
        .application(app)
        .title("Themer")
        .child(&buttonbox)
        .build();

    window.present();
}
