#[ path = "./helpers/themer_directory.rs" ] mod themer_directory;
#[ path = "./helpers/filename.rs" ] mod filename;
#[ path = "./helpers/file.rs" ] mod file;
#[ path = "./config.rs" ] mod config;
#[ path = "./ui.rs" ] mod ui;
#[ path = "./pywal.rs" ] mod pywal;
#[ path = "./template.rs" ] mod template;
use std::env;
use std::process;
use std::process::Command;

use template::make_template;
use template::make_directory_template;

use crate::config::Config;
use crate::config::Theme;

#[macro_use]
extern crate lazy_static;
extern crate mut_static;

extern crate fs_extra;


fn main() {
    let args: Vec<String> = env::args().collect();

    let mut config_file: String = String::from("");
    let mut get_pywal_status: bool = false;

    for i in 1..args.len() {
        if args[i] == "--config" {
            if args.len() - 1 < i + 1 {
                println!("Error: No config specified");
                process::exit(1);
            }
            config_file = String::from(&args[i+1]);
        } else if args[i] == "--defaults-check" {
            // TODO
        } else if args[i] == "--get-pywal-status" {
            // TODO
            get_pywal_status = true;
        }
    }

    if config_file.is_empty() {
        config_file = filename::construct_path("_THEMER_/config.toml").unwrap();
    }

    let config: Config = match config::get(config_file.as_str()) {
        Ok(c) => c,
        Err(e) => {
            println!("Error: {}, unable to continue.", e);
            process::exit(1);
        },
    };

    if get_pywal_status {
        print!("{}", &config.pywal_enabled);
        process::exit(0);
    }

    if config.pywal_enabled {
        pywal::main(&config, &config.pywal_main_color);
    } else {
        ui::main(&config);
    }
}

fn finish(config: Config, theme: Theme) {
    for (i, t) in config.templates.iter().enumerate() {
        match make_template(t, &theme) {
            Ok(_) => continue,
            Err(e) => println!("Error: {}, skipping template {}.", e, i + 1),
        }
    }

    for (i, d) in config.dirtemplates.iter().enumerate() {
        match make_directory_template(d, &theme) {
            Ok(_) => continue,
            Err(e) => println!("Error: {}, skipping template {}.", e, i + 1),
        }
    }

    for (i, d) in config.postrun_commands.iter().enumerate() {
        match Command::new("sh").arg("-c")
            .arg(d.as_str()).status() {
                Ok(_) => continue,
                Err(_) => println!("Error: Unable to run command, skipping command {}", i + 1),
            }
    }

    // Command::new("sh").arg("-c").arg("killall -USR1 st").status().unwrap();
}
