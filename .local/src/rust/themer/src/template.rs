use std::fs;
use std::path::PathBuf;
use crate::config::Template;
use crate::config::DirTemplate;
use crate::config::Theme;
use crate::file;
use crate::file::color_substitution_on_file;
use crate::filename::construct_path;

extern crate fs_extra;
use fs_extra::dir::copy;
use fs_extra::dir::remove;
use fs_extra::dir::CopyOptions;

extern crate walkdir;
use walkdir::WalkDir;

use regex::Regex;

pub fn make_template(template: &Template, theme: &Theme) -> Result<(), String> {

    let filename: String = construct_path(&template.filename)?;
    let content_filename: String = construct_path(&template.content_filename)?;

    if filename.is_empty() {
        return Err("Invalid template, no filename specified".to_owned());
    } else if content_filename.is_empty() {
        return Err("Invalid template, no content filename specified".to_owned());
    }

    let content: String = match fs::read_to_string(&content_filename) {
        Ok(s) => s,
        _ => return Err(format!("Unable to read from file {}", content_filename)),
    };

    match fs::write(&filename, content) {
        Ok(_) => return file::color_substitution_on_file(&filename, &theme),
        _ => return Err(format!("Unable to write to file {}", filename)),
    };
}

pub fn make_directory_template(template: &DirTemplate, theme: &Theme) -> Result<(), String> {
    let mut opts = CopyOptions::new();
    opts.copy_inside = true;
    let dir: String = construct_path(&template.directory)?;
    let dest: String = construct_path(&template.destination)?;

    remove(&dest).unwrap();
    copy(&dir, &dest, &opts).unwrap();

    let mut files: Vec<PathBuf> = Vec::new();
    let regex: Regex = Regex::new(&template.ignore_file_regex).unwrap();

    for entry in WalkDir::new(&dest).into_iter().filter_map(|e| e.ok()) {
        if !entry.file_type().is_dir() && !regex.is_match(entry.file_name().to_str().unwrap()) {
            files.push(entry.path().to_owned());
        }
    }

    for f in files {
        color_substitution_on_file(f.into_os_string().into_string().unwrap().as_str(), &theme)?;
    }

    Ok(())
}
