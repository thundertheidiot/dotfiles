use std::fs;
use toml::Value;

#[derive(Debug,Clone)]
pub struct Template {
    pub filename: String,
    pub content_filename: String,
}

#[derive(Debug,Clone)]
pub struct DirTemplate {
    pub directory: String,
    pub destination: String,
    pub ignore_file_regex: String,
}

#[derive(Debug,Clone)]
pub struct Theme {
    pub name: String,
    pub colors: Vec<String>,
}

#[derive(Debug,Clone)]
pub struct Config {
    pub pywal_enabled: bool,
    pub pywal_xresource_file: String,
    pub pywal_main_color: usize,
    // pub default_files_installed: bool,
    // pub default_files_directory: String,
    // pub default_files_destination: String,
    pub postrun_commands: Vec<String>,
    pub templates: Vec<Template>,
    pub dirtemplates: Vec<DirTemplate>,
    pub themes: Vec<Theme>,
}

impl Config {
    pub const fn new() -> Config {
        Config {
            pywal_enabled : false,
            pywal_xresource_file : String::new(),
            pywal_main_color : 5,
            // default_files_installed : false,
            // default_files_directory : String::new(),
            // default_files_destination : String::new(),
            postrun_commands: Vec::new(),
            templates: Vec::new(),
            dirtemplates: Vec::new(),
            themes: Vec::new(),
        }
    }
}

pub fn get(filename: &str) -> Result<Config, String> {
    let content = fs::read_to_string(filename).expect("Unable to read config.");
    let toml: Value = match toml::from_str(content.as_str()) {
        Ok(v) => v,
        Err(e) => return Err(e.to_string()),
    };

    let mut config: Config = Config::new();

    config.pywal_enabled = toml["themer"]["pywal_enabled"].as_bool().unwrap_or(false);
    config.pywal_xresource_file = toml["themer"]["pywal_xresources_file"].as_str().unwrap_or("").to_owned();
    config.pywal_main_color = toml["themer"]["pywal_main_color"].as_integer().unwrap_or(5) as usize;

    if let Some(array) = toml["themer"]["postrun"].as_array() {
        for item in array {
            if let Some(s) = item.as_str() {
                config.postrun_commands.push(s.to_owned());
            }
        }
    }

    config.dirtemplates = get_dirtemplates(&toml);
    config.templates = get_templates(&toml);
    config.themes = get_themes(&toml);

    Ok(config)
}

fn get_dirtemplates(toml: &Value) -> Vec<DirTemplate> {
    let templates = match toml.get("directory") {
        Some(Value::Array(arr)) => {
            arr.iter().map(|v| DirTemplate {
                directory: v.get("directory").unwrap().as_str().unwrap().to_owned(),
                destination: v.get("destination").unwrap().as_str().unwrap().to_owned(),
                ignore_file_regex: v.get("ignoreregex").unwrap().as_str().unwrap().to_owned(),
            })
            .collect::<Vec<DirTemplate>>()
        }
        _ => panic!("Invalid TOML format in config!")
    };

    return templates;
}

fn get_templates(toml: &Value) -> Vec<Template> {
    let templates = match toml.get("template") {
        Some(Value::Array(arr)) => {
            arr.iter().map(|v| Template {
                filename: v.get("filename").unwrap().as_str().unwrap().to_owned(),
                content_filename: v.get("content_file").unwrap().as_str().unwrap().to_owned(),
            })
            .collect::<Vec<Template>>()
        }
        _ => panic!("Invalid TOML format in config!")
    };

    return templates;
}

fn get_themes(toml: &Value) -> Vec<Theme> {
    let mut themes = match toml.get("theme") {
        Some(Value::Array(arr)) => {
            arr.iter().map(|v| Theme {
                name: v.get("name").unwrap().as_str().unwrap().to_owned(),
                colors: Vec::from([
                    v.get("color_0").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_1").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_2").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_3").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_4").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_5").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_6").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_7").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_8").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_9").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_10").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_11").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_12").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_13").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_14").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("color_15").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("background").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("foreground").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                    v.get("maincolor").unwrap().as_str().unwrap().to_owned().replace("#", ""),
                ])
            })
            .collect::<Vec<Theme>>()
        }
        _ => panic!("Invalid TOML format in config!")
    };

    for t in &mut themes {
        let index: usize = t.colors[18].parse::<usize>().unwrap();
        t.colors[18] = t.colors[index].clone();
    }

    return themes;
}
