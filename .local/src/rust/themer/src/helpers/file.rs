use std::{fs, process::Command};
use regex::Regex;

use crate::config::Theme;

pub fn substitute_string_in_file(filename: &str, from: &str, to: &str) -> Result<(), String> {
    
    let content: String = match fs::read_to_string(filename) {
        Ok(s) => s,
        _ => return Err(format!("Unable to read from file {}", filename)),
    };

    let new: String = content.replace(from, to);

    match fs::write(filename, new) {
        Ok(_) => return Ok(()),
        _ => return Err(format!("Unable to write to file {}", filename)),
    };
}

pub fn color_substitution_on_file(filename: &str, theme: &Theme) -> Result<(), String> {
    let colors: &Vec<String> = &theme.colors;

    substitute_string_in_file(filename, "color_15", colors[15].as_str())?;
    substitute_string_in_file(filename, "color_14", colors[14].as_str())?;
    substitute_string_in_file(filename, "color_13", colors[13].as_str())?;
    substitute_string_in_file(filename, "color_12", colors[12].as_str())?;
    substitute_string_in_file(filename, "color_11", colors[11].as_str())?;
    substitute_string_in_file(filename, "color_10", colors[10].as_str())?;
    substitute_string_in_file(filename, "color_9", colors[9].as_str())?;
    substitute_string_in_file(filename, "color_8", colors[8].as_str())?;
    substitute_string_in_file(filename, "color_7", colors[7].as_str())?;
    substitute_string_in_file(filename, "color_6", colors[6].as_str())?;
    substitute_string_in_file(filename, "color_5", colors[5].as_str())?;
    substitute_string_in_file(filename, "color_4", colors[4].as_str())?;
    substitute_string_in_file(filename, "color_3", colors[3].as_str())?;
    substitute_string_in_file(filename, "color_2", colors[2].as_str())?;
    substitute_string_in_file(filename, "color_1", colors[1].as_str())?;
    substitute_string_in_file(filename, "color_0", colors[0].as_str())?;

    substitute_string_in_file(filename, "color_bg", colors[16].as_str())?;
    substitute_string_in_file(filename, "color_fg", colors[17].as_str())?;
    substitute_string_in_file(filename, "color_main", colors[18].as_str())?;
    
    // Themer magic
    let content: String = match fs::read_to_string(filename) {
        Ok(v) => v,
        _ => return Err(format!("Unable to read from file {}", filename)),
    };
    let regex = Regex::new(r"themer\{.*\}").unwrap();

    for m in regex.find_iter(content.as_str()) {
        let result = m.as_str().replace("themer{", "")[0..m.as_str().len() - 8].to_string(); // themer{ = 7 characters - 1
        let commandregex = Regex::new(r":.*").unwrap();
        let contentregex = Regex::new(r".*:").unwrap();

        let content: String;
        let command: String;

        if let Some(capture) = commandregex.find(result.as_str()) {
            content = result.replace(capture.as_str(), ""); 
        } else {
            continue;
        }
        
        if let Some(capture) = contentregex.find(result.as_str()) {
            command = result.replace(capture.as_str(), ""); 
        } else {
            continue;
        }

        let command_comregex = Regex::new(r"com\(.*\)").unwrap();

        if let Some(capture) = command_comregex.find(command.as_str()) {
            let com = capture.as_str().replace("com((", "").replace("))", "");
            let output: String = match Command::new("sh").arg("-c")
                .arg(com.as_str()).output() {
                    Ok(v) => String::from_utf8(v.stdout).unwrap(),
                    Err(_) => {
                        println!("Error: Failed to run command \"{}\", continuing.", com.as_str()); 
                        continue
                    },
            };

            substitute_string_in_file(filename, m.as_str(), output.as_str())?;
            continue;
        }

        let command_addregex = Regex::new(r"\+[0-9]*\%").unwrap();

        if let Some(capture) = command_addregex.find(command.as_str()) {
            let percentage: f32 = capture.as_str().replace("+", "").replace("%", "").parse::<f32>().unwrap() / 100.0;


            let r: u8 = u8::from_str_radix(&content[0..2], 16).unwrap();
            let g: u8 = u8::from_str_radix(&content[2..4], 16).unwrap();
            let b: u8 = u8::from_str_radix(&content[4..6], 16).unwrap();

            let new_r: u8 = (r as f32 * (1.0 + percentage)) as u8;
            let new_b: u8 = (g as f32 * (1.0 + percentage)) as u8;
            let new_g: u8 = (b as f32 * (1.0 + percentage)) as u8;
            
            let color = format!("{:x}{:x}{:x}", new_r, new_b, new_g);
            
            substitute_string_in_file(filename, m.as_str(), &color)?;
            continue;

        }
        
        let command_addregex = Regex::new(r"\-[0-9]*\%").unwrap();

        if let Some(capture) = command_addregex.find(command.as_str()) {
            let percentage: f32 = capture.as_str().replace("-", "").replace("%", "").parse::<f32>().unwrap() / 100.0;


            let r: u8 = u8::from_str_radix(&content[0..2], 16).unwrap();
            let g: u8 = u8::from_str_radix(&content[2..4], 16).unwrap();
            let b: u8 = u8::from_str_radix(&content[4..6], 16).unwrap();

            let new_r: u8 = (r as f32 * (1.0 - percentage)) as u8;
            let new_b: u8 = (g as f32 * (1.0 - percentage)) as u8;
            let new_g: u8 = (b as f32 * (1.0 - percentage)) as u8;
            
            let color = format!("{:x}{:x}{:x}", new_r, new_b, new_g);
            
            substitute_string_in_file(filename, m.as_str(), &color)?;
            continue;

        }

    }

    Ok(())
}
