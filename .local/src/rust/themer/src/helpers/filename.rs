use std::env;
use crate::themer_directory;
use regex::Regex;

pub fn construct_path(filename: &str) -> Result<String, String> {
    let home = env::var("HOME").unwrap();
    let config = env::var("XDG_CONFIG_HOME").unwrap_or(format!("{}{}", home, "/.config/"));
    let themer = themer_directory::get();

    let mut s: String = filename.to_owned();

    // let var_pattern = r"\$([A-Z]|[a-z]|[0-9]|_)*"; 
    let regex = Regex::new(r"\$\w*").unwrap();

    while let Some(captures) = regex.find(s.as_str()) {
        let varname: String = captures.as_str().replace("$", "");

        match env::var(&varname) {
            Ok(var) => s = s.replace(captures.as_str(), var.as_str()),
            _ => return Err(format!("Environment variable ${} is not set", &varname).to_owned()),
        };

    }
    
    s = s.replace("_HOME_", home.as_str());
    s = s.replace("_CONFIG_", config.as_str());
    s = s.replace("_THEMER_", themer.as_str());

    return Ok(s);
}
