use std::env;

pub fn get() -> String {
    return env::var("XDG_CONFIG_HOME")
        .unwrap_or(format!("{}{}", env::var("HOME").expect("Environment variable $HOME not set"), "/.config/"));
}
