#[path = "./xdg_config_home.rs"] mod xdg_config_home;
use std::env;

pub fn get() -> String {
    let c: String = xdg_config_home::get();
 
    return env::var("THEMER_DIRECTORY")
        .unwrap_or(format!("{}{}", c, "/themer/"));
}
