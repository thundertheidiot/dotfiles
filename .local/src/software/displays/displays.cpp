#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <string.h>
#include <limits>
#include "cpptoml.h"
// using namespace std;

class Display {
	public:
		std::string name = "";
		std::string mode = "";
		int refresh = -1;
		bool primary = false;
		std::string scale = "";
		bool noscale = false;
		std::string extra = "";
		bool off = false;
};

std::vector<Display*> display_pointers = {};

std::string operating_display_name;

Display* get_display_by_name(std::string name) {

	for (Display* d : display_pointers) {
		if (d->name == name) {
			return d;
		}
	}

	Display* n;
	return n;
}

void print_information() {
	for (Display* d : display_pointers) {
		if (d->off) {
			continue;
		}
		std::cout << "Display: " << d->name << std::endl;
		std::cout << "    Mode: " << d->mode << std::endl;
		if (d->refresh != -1) {
			std::cout << "    Refresh: " << d->refresh << std::endl;
		}
		if (!d->scale.empty()) {
			std::cout << "    Scale: " << d->scale << std::endl;
		}
		if (!d->extra.empty()) {
			std::cout << "    Extra opts: " << d->extra << std::endl;
		}
	}
}

void create_modelines(Display* d, std::vector<std::string> modes) {
	for (int i = 0; i < modes.size(); i += 2) {
		std::string newmode = "xrandr --newmode " + modes[i] + " " + modes[i + 1];
		std::string addmode = "xrandr --addmode " + d->name + " " + modes[i];

		std::system(newmode.c_str());
		std::system(addmode.c_str());
	}
}

void parse_toml_for_displays(std::string file) {
	auto config = cpptoml::parse_file(file);
	auto display_array = config->get_table_array("display");

	for (const auto& table : *display_array)
	{
		auto name = table->get_as<std::string>("name");
		auto mode = table->get_as<std::string>("mode");
		auto primary = table->get_as<int>("primary").value_or(0);
		auto refresh = table->get_as<int>("refresh").value_or(-1);
		auto off = table->get_as<int>("off").value_or(0);
		auto scale = table->get_as<std::string>("scale_from");
		auto extra = table->get_as<std::string>("extra");
		auto modelines = table->get_array_of<std::string>("modelines");

		Display* d = new Display;

		d->name = *name;
		d->mode = *mode;
		if (primary == 1) { d->primary = true; };	
		if (refresh != -1) { d->refresh = refresh; };	
		if (off == 1) { d->off = true; };
		d->scale = *scale;
		d->extra = *extra;

		create_modelines(d, *modelines);

		display_pointers.push_back(d);

	}

}

void set_settings() {
	std::string command = "xrandr ";

	for (Display* d : display_pointers) {
		command += "--output " + d->name + " ";
		if (d->off) {
			command += " --off ";
			continue;
		}
		if (d->primary) {
			command += " --primary ";
		}
		if (!d->mode.empty()) {
			command += " --mode " + d->mode + " ";
		}
		if (!d->scale.empty()) {
			command += " --scale-from " + d->scale + " ";
		}
		if (!d->extra.empty()) {
			command += " " + d->extra + " ";
		}
		if (d->refresh != -1) {
			command += " --refresh " + std::to_string(d->refresh) + " ";
		}
		
	}

	// TODO run command
	// std::cout << command << std::endl;
	std::system(command.c_str());
}

int main(int argc, char* argv[]) {

	std::string config;
	bool printinfo = false;

	for (int i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "--config") || !strcmp(argv[i], "-c")) {
			config = std::string(argv[++i]);
			parse_toml_for_displays(config);
		}
		if (!strcmp(argv[i], "--info") || !strcmp(argv[i], "-i")) {
			printinfo = true;
		}
		if (!strcmp(argv[i], "--display")) {
			operating_display_name = std::string(argv[++i]);
		}
		if (!strcmp(argv[i], "--mode")) {
			Display *d = get_display_by_name(operating_display_name);
			d->mode= std::string(argv[++i]);
		}
		if (!strcmp(argv[i], "--refresh")) {
			Display *d = get_display_by_name(operating_display_name);
			d->refresh = atoi(argv[++i]);
		}
		if (!strcmp(argv[i], "--scale")) {
			Display *d = get_display_by_name(operating_display_name);
			d->scale = std::string(argv[++i]);
		}
		if (!strcmp(argv[i], "--extra-opts")) {
			Display *d = get_display_by_name(operating_display_name);
			d->extra = std::string(argv[++i]);
		}
		if (!strcmp(argv[i], "--off")) {
			Display *d = get_display_by_name(operating_display_name);
			d->off = true;
		}
	}

	if (config.empty()) {
		std::cout << "Error: No config specified" << std::endl;
		return 0;
	}
	if (printinfo) { print_information(); };
	set_settings();

	return 0;
}
