# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
config: ConfigAPI = config  # noqa: F821 pylint: disable=E0602,C0103
c: ConfigContainer = c  # noqa: F821 pylint: disable=E0602,C0103

config.load_autoconfig(False)

c.content.javascript.enabled = True
c.scrolling.smooth = False

c.content.blocking.method = "adblock"
c.content.blocking.adblock.lists = [
        # Easylist
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
        "https://easylist.to/easylist/fanboy-social.txt",
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt",

        #
        ]


config.bind('=', 'zoom-in')
config.bind('-', 'zoom-out')

config.bind(' v', 'hint links spawn mpv {hint-url}')
