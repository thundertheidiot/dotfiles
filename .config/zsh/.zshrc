export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"

# I use oh-my-zsh again, i don't care if it's "bad" or "bloated", it literally just works on everything.
# Go use someone else's dotfiles if you hate me for this.

export ZSH="$XDG_CONFIG_HOME/zsh/oh-my-zsh"
export ZSH_CUSTOM="$ZDOTDIR"

ZSH_THEME="minimal-prompt"

# oh-my-zsh plugins
plugins=(vi-mode)


install-oh-my-zsh() {
  echo "It seems like oh my zsh isn't installed, it will be installed now.\n"
	git clone https://github.com/ohmyzsh/ohmyzsh.git $ZSH/ && zsh
}

[ ! -d $ZSH/ ] && install-oh-my-zsh # If $ZSH directory doesn't exist, run function to install oh-my-zsh

source $ZSH/oh-my-zsh.sh

fetch() { # Only run neofetch if it exists.
	#which fastfetch >/dev/null && fastfetch 
	which pfetch >/dev/null && pfetch
}

# Cleanup
alias yarn='yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'
alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
alias dosbox='dosbox -conf="$XDG_CONFIG_HOME"/dosbox/dosbox.conf'
alias adb='HOME="$XDG_DATA_HOME"/android adb'


# Dotfiles git wrapper
alias cfg="/usr/bin/git --git-dir=$HOME/.local/src/dotfiles/ --work-tree=$HOME/"

alias z="clear && fetch"

alias ondemand="sudo cpupower frequency-set --governor ondemand && sudo cpupower frequency-set --max 4000000"
alias powersave3="sudo cpupower frequency-set --governor powersave && sudo cpupower frequency-set --max 3000000"
alias powersave="sudo cpupower frequency-set --governor powersave && sudo cpupower frequency-set --max 4000000"
alias 4ghz="sudo cpupower frequency-set --max 4000000"
alias 3ghz="sudo cpupower frequency-set --max 3000000"
alias 28ghz="sudo cpupower frequency-set --max 2800000"

alias e="emacsclient -s /tmp/emacs1000/server -c"
alias dc="docker-compose"

alias q="exit"

source "$ZDOTDIR/custom"

clear && fetch
