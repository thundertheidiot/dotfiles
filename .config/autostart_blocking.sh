#!/bin/sh

xsetroot -cursor_name left_ptr & # Set default cursor image on desktop
xset r rate 300 50 
setxkbmap "us" # Set keyboard layout
setxkbmap -option caps:escape # Fix capslock
displays & # Setup displays

xrdb -load "$XRESOURCES" # Load xresources

themer defaults_check &
gnome-keyring-daemon --start --daemonize --components ssh & # Gnome keyring

xfsettingsd &# Enable using xfce settings
/usr/lib/xfce4/notifyd/xfce4-notifyd & # Xfce notification daemon

setbg & # Set the wallpaper
unclutter & # Hide cursor after inactivity
gajim & # Best xmpp client
mpd & # Music player daemon
xcompmgr &
nm-applet & # Networkmanager applet

easyeffects & # Audio effects
ckb-next -b & # Corsair mouse and keyboard driver

dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY # Fix slow gtk app starting

dwmblocks &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & # Polkit authentication
mailtimer & 
[ -f "$XDG_CONFIG_HOME/autostart_$(uname -n).sh" ] && "$XDG_CONFIG_HOME/autostart_$(uname -n).sh"
