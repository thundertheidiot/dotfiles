return {
	{
		'folke/which-key.nvim',
		event = 'VeryLazy',
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
		end,
		opts = {

		}
	},
	{
		'dstein64/vim-startuptime',
	},
	{
		'nvim-telescope/telescope.nvim',
		branch = '0.1.x',
		dependencies = { 'nvim-lua/plenary.nvim' },
		lazy = true,
		init = function()
			require('telescope').setup{
				defaults = {
					mappings = {
						i = {
							["<C-k>"] = "move_selection_previous",
							["<C-j>"] = "move_selection_next",
						}

					},
				},
			}
		end,
	},
	{
		'romgrk/barbar.nvim',
		dependencies = {
			'nvim-tree/nvim-web-devicons',
			'lewis6991/gitsigns.nvim',
		},
		init = function() vim.g.barbar_auto_setup = false end,
		opts = {
				animation = true,
				auto_hide = false,
				tabpages = true,
				clickable = true,

				focus_on_close = 'left',
				hide = {extensions = false, inactive = false},
				highlight_visible = true,

				icons = {
					button = '',

					buffer_index = false,
					buffer_number = false,
					
					diagnostics = {
						[vim.diagnostic.severity.ERROR] = {enabled = true, icon = 'ﬀ'},
						[vim.diagnostic.severity.WARN] = {enabled = false},
						[vim.diagnostic.severity.INFO] = {enabled = false},
      					[vim.diagnostic.severity.HINT] = {enabled = true},
					},

					gitsigns = {
      					added = {enabled = true, icon = '+'},
      					changed = {enabled = true, icon = '~'},
      					deleted = {enabled = true, icon = '-'},
    				},

					filetype = {
						custom_colors = false,
						enabled = true,
					},

					-- separator = {left = '▎', right = ''},

					separator_at_end = true,

					modified = {button = '●'},
    				pinned = {button = '', filename = true},

					preset = 'default',

				},

				insert_at_end = true,
				insert_at_start = false,

				maximum_padding = 1,
				minimum_padding = 1,

				maximum_length = 30,
				minimum_length = 0,

				letters = 'asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP',

				no_name_title = nil,
		},
		version = '^1.0.0',
	},
	{
		'nvim-tree/nvim-tree.lua',
		dependencies = { 'nvim-tree/nvim-web-devicons' },
		init = function()
			require('nvim-tree').setup()
			-- local api = require('nvim-tree.api')
			-- vim.keymap.set('n', '<leader>.', api.tree.toggle, {})
		end,
	},
	{
		'ap/vim-css-color'
	},
	-- {
		-- 'lukas-reineke/indent-blankline.nvim',
		-- init = function()
			-- require('ibl').setup({})
		-- end,
	-- },
	{
		'nvim-treesitter/nvim-treesitter',
		dependencies = { 'luckasRanarison/tree-sitter-hypr' },
		build = function() vim.cmd(':TSUpdate') end,
		init = function()
			local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
			parser_config.hypr = {
				install_info = {
					url = "https://github.com/luckasRanarison/tree-sitter-hypr",
					files = { "src/parser.c" },
					branch = "master",
				},
				filetype = "hypr",
			}

			require'nvim-treesitter.configs'.setup {
  				ensure_installed = { "javascript", "c", "lua", "vim", "rust", "cpp", "css", "gdscript", "vimdoc", "latex", "hypr" },

  				sync_install = false,

  				auto_install = true,

				-- ignore_install = { "latex" },
				--  disable = { "latex" },

  				highlight = {
    				enable = true,
    				additional_vim_regex_highlighting = true,
  				},
			}

		end,
	},
	-- {
 --  		'VonHeikemen/lsp-zero.nvim',
 --  		branch = 'v2.x',
 --  		dependencies = {
 --    		-- LSP Support
 --    		{'neovim/nvim-lspconfig'},             -- Required
 --    		{                                      -- Optional
 --      			'williamboman/mason.nvim',
 --      			build = function()
 --        		pcall(vim.cmd, 'MasonUpdate')
 --      			end,
 --    		},
 --    		{'williamboman/mason-lspconfig.nvim'}, -- Optional
	--
 --    		-- Autocompletion
 --    		{'hrsh7th/nvim-cmp'},     -- Required
 --    		{'hrsh7th/cmp-nvim-lsp'}, -- Required
 --    		{'L3MON4D3/LuaSnip'},     -- Required
	--
	-- 		{'simrat39/rust-tools.nvim'}
 --  		},
	-- 	init = function()
	-- 		vim.cmd.highlight({"link", "LspInlayHint", "Comment"})
	--
	-- 		local lsp = require("lsp-zero").preset({})
	--
	-- 		lsp.ensure_installed({
	-- 			'lua_ls',
	-- 			'clangd',
	-- 			'tsserver',
	-- 			'rust_analyzer'
	-- 		})
	--
	-- 		lsp.skip_server_setup({'rust_analyzer'})
	--
	-- 		lsp.on_attach(function(client, bufnr)
	-- 			lsp.default_keymaps({buffer = bufnr})
	-- 		end)
	--
	-- 		require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())
	--
	-- 		lsp.setup()
	--
	-- 		local rust_tools = require('rust-tools')
	--
	-- 		rust_tools.setup({
	-- 			server = {
	-- 				on_attach = function(_, bufnr)
	-- 					vim.keymap.set('n', '<leader>ca', rust_tools.hover_actions.hover_actions, {buffer = bufnr})
	-- 				end
	-- 			}
	-- 		})
	--
	-- 		local cmp = require("cmp")
	-- 		--local cmp_action = require("lsp-zero").cmp_action()
	--
	-- 		cmp.setup({
	-- 			mapping = {
	-- 				["<CR>"] = cmp.mapping.confirm({select = false}),
 --    				['<tab>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
 --    				['<s-tab>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
	-- 			},
	--
	-- 			sources = {
	-- 				{ name = "crates" },
	-- 			}
	-- 		})
	--
	-- 		require("lspconfig").gdscript.setup({
	-- 			filetypes = {
	-- 				"gdscript",
	-- 			},
	-- 		})
	-- 	end,
	-- },
	-- {
	-- 	'windwp/nvim-autopairs',
	-- 	event = "InsertEnter",
	-- 	opts = {}
	-- },
	{
		'nvim-lualine/lualine.nvim',
		lazy = false,
		dependencies = {
			'nvim-tree/nvim-web-devicons',
		},
		init = function()
			require('lualine').setup()
		end,
	},
	{
		'dstein64/vim-startuptime',
		lazy = true,
		cmd = 'StartupTime',
	},
	{
		'lervag/vimtex',
		lazy = true,
		ft = 'tex',
		init = function()
			vim.g.tex_flavor = 'latex'
			vim.g.vimtex_view_general_viewer = 'setsid -f zathura'

			vim.cmd("let g:vimtex_compiler_latexmk = { 'continuous' : 1, 'build_dir' : 'build' }")
			vim.cmd("let g:vimtex_quickfix_ignore_filters = [ 'Underfull', 'Overfull']")

			vim.api.nvim_create_autocmd("User", {
				pattern = "VimtexEventInitPost",
				group = augroup,
				command = "VimtexCompile",
			})

			vim.api.nvim_create_autocmd("User", {
				pattern = "VimtexEventInitPost",
				group = augroup,
				command = "TSBufDisable highlight",
			})

			vim.api.nvim_create_autocmd("User", {
				pattern = "VimtexEventCompileStopped",
				group = augroup,
				command = "VimtexClean",
			})
		end,
	},
	{
		'sirver/ultisnips',
		lazy = true,
		ft = 'tex',
		init = function()
			vim.g.UltiSnipsExpandTrigger = '<tab>'
			vim.g.UltiSnipsJumpForwardTrigger = '<tab>'
			vim.g.UltiSnipsJumpBackwardTrigger = '<s-tab>'
		end,
	},
	{
		'numToStr/Comment.nvim',
		init = function()
			require('Comment').setup()
		end,
	},
	{
		'NvChad/nvterm',
		config = function()
			require("nvterm").setup()
		end,
	},
	{
		'Saecki/crates.nvim',
		dependencies = { 'nvim-lua/plenary.nvim' },
    	init = function()
        	require('crates').setup()
    	end,
	}
	-- {
	-- 	'folke/noice.nvim',
	-- 	config = function()
	-- 		require('noice').setup({
	-- 			cmdline = {
	-- 				enabled = true,
	-- 				view = 'cmdline_popup',
	-- 			},
	-- 			popupmenu = {
	-- 				enabled = true,
	-- 				backed = 'nui',
	-- 			},
	-- 			routes = {
	-- 				{
	-- 					filter = { event = "cmdline", find = "UltiSnips#ExpandSnippet", },
	-- 					opts = { skip = true },
	-- 				},
	-- 				{
	-- 					filter = { event = "cmdline", find = "UltiSnips#ListSnippets", },
	-- 					opts = { skip = true },
	-- 				},
	-- 			},
	-- 		})
	-- 	end,
	-- 	dependencies = {
	-- 		'MunifTanjim/nui.nvim',
	-- 		'rcarriga/nvim-notify',
	-- 	},
	-- },
}
