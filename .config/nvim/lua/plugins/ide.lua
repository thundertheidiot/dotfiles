return {
	{
		'windwp/nvim-autopairs',
		init = function()
			require('nvim-autopairs').setup({ map_bs = false })
		end
	},
	{
		'lukas-reineke/indent-blankline.nvim',
		main = 'ibl',
		opts = {},
	},
	{
		'neovim/nvim-lspconfig',
		dependencies = {'williamboman/mason.nvim',
			'williamboman/mason-lspconfig.nvim',
			'ms-jpq/coq_nvim',
			'simrat39/rust-tools.nvim',
			-- 'mrcjkb/rustaceanvim',
		},
		build = function() vim.cmd('COQdeps') end,
		init = function()
			local lspconfig = require('lspconfig')

			vim.api.nvim_create_autocmd('LspAttach', {
  				desc = 'LSP actions',
				callback = require('config/keybindings').lsp_bindings,
			})

			local coq = require('coq')

			require('mason').setup({})
			require('mason-lspconfig').setup({
				ensure_installed = {'lua_ls', 'clangd', 'tsserver', 'rust_analyzer'},
				handlers = {
					function(server_name)
						lspconfig[server_name].setup(coq.lsp_ensure_capabilities({}))
					end,
					["rust_analyzer"] = function()
						local rust_tools = require('rust-tools')
						rust_tools.setup({
							server = {
								on_attach = function(_, bufnr)
									vim.keymap.set('n', '<leader>ca', rust_tools.hover_actions.hover_actions, {buffer = bufnr})
								end
							}
						})
					end
				},
			})


		end,
	}
}
