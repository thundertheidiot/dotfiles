vim.g.mapleader = " "
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.cmd("set clipboard+=unnamedplus")
vim.cmd("set number relativenumber")
vim.opt.scrolloff = 10
vim.opt.syntax = "on"
vim.opt.mouse = ""

vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.autochdir = true

vim.g.coq_settings = {
	["auto_start"] = 'shut-up',
	["display.preview.border"] = "shadow",
	["keymap.recommended"] = false,
	["display.pum.fast_close"] = false,
	["completion.replace_prefix_threshold"] = 5,
	["completion.replace_suffix_threshold"] = 5,
	["clients.buffers.enabled"] = false,
	["clients.registers.enabled"] = false,
}

vim.api.nvim_create_augroup("AutoFormat", {})

vim.api.nvim_create_autocmd(
	"BufWritePost",
	{
		pattern = "*.rs",
		group = "AutoFormat",
		callback = function()
			vim.cmd('silent exec "!cargo fmt"')
		end,
	}
)
