local M = {}

function M.register()

	vim.keymap.set('n', '<SPACE>', '<Nop>')
	
	vim.keymap.set('n', '<leader>j', '<C-w><C-j>')
	vim.keymap.set('n', '<leader>k', '<C-w><C-k>')
	vim.keymap.set('n', '<leader>h', '<C-w><C-h>')
	vim.keymap.set('n', '<leader>l', '<C-w><C-l>')
	
	vim.keymap.set('t', '<esc>', '<C-\\><C-n>')

	local wk = require("which-key")

	local telescope_builtin = require("telescope.builtin")
	local nvim_tree_api = require("nvim-tree.api")
	local nvterm_terminal = require("nvterm.terminal")

	wk.register({
		["<leader>f"] = { name = "+file" },
		["<leader>ff"] = { telescope_builtin.find_files, "Find files" },
		["<leader>fg"] = { telescope_builtin.git_files, "Find git files" },
		["<leader>fs"] = { telescope_builtin.live_grep, "Live grep" },
		["<leader>fr"] = { telescope_builtin.grep_string, "Grep" },

		["<leader>."] = { nvim_tree_api.tree.toggle, "File manager"},
	
		["<leader>v"] = { "<cmd>VimtexCompile<cr>" , "Vimtex compile"},


		["<leader>t"] = { function()
			nvterm_terminal.toggle("horizontal")
		end, "Toggle terminal" },
	
		["<leader>c"] = { name = "+cargo" },
		["<leader>cb"] = { function()
			nvterm_terminal.send("cargo build", "horizontal")
		end, "Cargo build" },
		["<leader>cbr"] = { function()
			nvterm_terminal.send("cargo build --release", "horizontal")
		end, "Cargo build release" },
	
		["<leader>cr"] = { function()
			nvterm_terminal.send("cargo run", "horizontal")
		end, "Cargo run" },
		["<leader>crr"] = { function()
			nvterm_terminal.send("cargo run --release", "horizontal")
		end, "Cargo run release" },
		["."] = { '<Cmd>BufferNext<CR>', "Next buffer" },
		[","] = { '<Cmd>BufferPrevious<CR>', "Previous buffer" },

		["<leader>sp"] = { '<Cmd>sp<CR>', "Split horizontally" },
		["<leader>vsp"] = { '<Cmd>vsp<CR>', "Split vertically" },

		["<leader>wq"] = { '<Cmd>wq<CR>', "Write and quit"},
		["<leader>q"] = { '<Cmd>BufferClose<CR>', "Quit"},
	})

	local nvim_autopairs = require('nvim-autopairs')

	vim.api.nvim_set_keymap('i', '<esc>', [[pumvisible() ? "<c-e><esc>" : "<esc>"]], { expr = true, noremap = true })
	vim.api.nvim_set_keymap('i', '<c-c>', [[pumvisible() ? "<c-e><c-c>" : "<c-c>"]], { expr = true, noremap = true })
	vim.api.nvim_set_keymap('i', '<tab>', [[pumvisible() ? "<c-n>" : "<tab>"]], { expr = true, noremap = true })
	vim.api.nvim_set_keymap('i', '<s-tab>', [[pumvisible() ? "<c-p>" : "<bs>"]], { expr = true, noremap = true })
			
	_G.MUtils = {}

	MUtils.CR = function()
		if vim.fn.pumvisible() ~= 0 then
			if vim.fn.complete_info({ 'selected' }).selected ~= -1 then
				return nvim_autopairs.esc('<c-y>')
			else
				return nvim_autopairs.esc('<c-e>') .. nvim_autopairs.autopairs_cr()
			end
		else
			return nvim_autopairs.autopairs_cr()
		end
	end
	vim.api.nvim_set_keymap('i', '<cr>', 'v:lua.MUtils.CR()', { expr = true, noremap = true })

	MUtils.BS = function()
		if vim.fn.pumvisible() ~= 0 and vim.fn.complete_info({ 'mode' }).mode == 'eval' then
			return nvim_autopairs.esc('<c-e>') .. nvim_autopairs.autopairs_bs()
		else
			return nvim_autopairs.autopairs_bs()
		end
	end
	vim.api.nvim_set_keymap('i', '<bs>', 'v:lua.MUtils.BS()', { expr = true, noremap = true })
end

function M.lsp_bindings(event)
	local opts = {buffer = event.buf}

    vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
    vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
    vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
    vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
    vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
    vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)
    vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
    vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
    vim.keymap.set({'n', 'x'}, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
    vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
    vim.keymap.set('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)

    vim.keymap.set('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<cr>', opts)
    vim.keymap.set('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>', opts)
    vim.keymap.set('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>', opts)
end

return M
