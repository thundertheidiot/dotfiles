#!/bin/sh

Xephyr -screen 1024x800 +xinerama :420 &

export DISPLAY=:420

sleep 0.5 && DISPLAY=:420 awesome
